Run Tomcat6 container with following command: 

    docker run -d -p 4922:22 -p 9090:8080 markojovicic/tomcat-6

You can ssh into container using ssh:

    ssh -p 4922 root@localhost

Root password is **admin**.

Tomcat web admin is available on [http://localhost:9090][1] 
u/p: admin/admin

----------

It runs Tomcat6 using Open JDK 1.7.


  [1]: http://localhost:9090
