#!/bin/bash
set -e

# Author: Marko Jovicic <markojovicic@gmail.com>
#
# tomcat6 installation. openjdk-7
#

# Sets "Europe/Belgrade" time zone
cp clock /etc/sysconfig/clock

yum install -y openssh-server

yum install -y java-1.7.0-openjdk
yum install -y tomcat6
yum install -y tomcat6-webapps
yum install -y tomcat6-admin-webapps

# preconfigured
rm -rf /etc/tomcat6/*
cp -r tomcat6-conf/* /etc/tomcat6
cp ojdbc6.jar /usr/share/tomcat6/lib/

chkconfig --add tomcat6
chkconfig --level 2345 tomcat6 on




