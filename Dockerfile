
FROM centos:centos6
MAINTAINER Marko Jovicic <markojovicic@gmail.com>

ADD setup /tmp/setup

WORKDIR /tmp/setup

RUN ./setup.sh

RUN service sshd start
RUN service sshd stop

RUN printf "admin\nadmin\n" | passwd

EXPOSE 22
EXPOSE 8080

CMD service tomcat6 start; \
    /usr/sbin/sshd -D

